<?php
require_once 'reader.php';

class PlayerLogWriter extends PlayerLogReader {
    function joinEvent($time, $newTeam) {
        echo "joinEvent," . implode(",", func_get_args()) . PHP_EOL;
    }
    function quitEvent($time, $oldFlag, $oldPowers, $oldTeam) {
        echo "quitEvent," . implode(",", func_get_args()) . PHP_EOL;
    }
    function switchEvent($time, $oldFlag, $powers, $newTeam) {
        echo "switchEvent," . implode(",", func_get_args()) . PHP_EOL;
    }
    function grabEvent($time, $newFlag, $powers, $team) {
        echo "grabEvent," . implode(",", func_get_args()) . PHP_EOL;
    }
    function captureEvent($time, $oldFlag, $powers, $team) {
        echo "captureEvent," . implode(",", func_get_args()) . PHP_EOL;
    }
    function flaglessCaptureEvent($time, $flag, $powers, $team) {
        echo "flaglessCaptureEvent," . implode(",", func_get_args()) . PHP_EOL;
    }
    function powerupEvent($time, $flag, $powerUp, $newPowers, $team) {
        echo "powerupEvent," . implode(",", func_get_args()) . PHP_EOL;
    }
    function duplicatePowerupEvent($time, $flag, $powers, $team) {
        echo "duplicatePowerupEvent," . implode(",", func_get_args()) . PHP_EOL;
    }
    function powerdownEvent($time, $flag, $powerDown, $newPowers, $team) {
        echo "powerdownEvent," . implode(",", func_get_args()) . PHP_EOL;
    }
    function returnEvent($time, $flag, $powers, $team) {
        echo "returnEvent," . implode(",", func_get_args()) . PHP_EOL;
    }
    function tagEvent($time, $flag, $powers, $team) {
        echo "tagEvent," . implode(",", func_get_args()) . PHP_EOL;
    }
    function dropEvent($time, $oldFlag, $powers, $team) {
        echo "dropEvent," . implode(",", func_get_args()) . PHP_EOL;
    }
    function popEvent($time, $powers, $team) {
        echo "popEvent," . implode(",", func_get_args()) . PHP_EOL;
    }
    function startPreventEvent($time, $flag, $powers, $team) {
        echo "startPreventEvent," . implode(",", func_get_args()) . PHP_EOL;
    }
    function stopPreventEvent($time, $flag, $powers, $team) {
        echo "stopPreventEvent," . implode(",", func_get_args()) . PHP_EOL;
    }
    function startButtonEvent($time, $flag, $powers, $team) {
        echo "startButtonEvent" . implode(",", func_get_args()) . PHP_EOL;
    }
    function stopButtonEvent($time, $flag, $powers, $team) {
        echo "stopButtonEvent," . implode(",", func_get_args()) . PHP_EOL;
    }
    function startBlockEvent($time, $flag, $powers, $team) {
        echo "startBlockEvent," . implode(",", func_get_args()) . PHP_EOL;
    }
    function stopBlockEvent($time, $flag, $powers, $team) {
        echo "stopBlockEvent," . implode(",", func_get_args()) . PHP_EOL;
    }
    function endEvent($time, $flag, $powers, $team) {
        echo "endEvent," . implode(",", func_get_args()) . PHP_EOL;
    }
}

$gameid = $argv[1];
$playerid = $argv[2];
$data = json_decode(file_get_contents('https://tagpro.eu/data/?match=' . $gameid), true);
$event = base64_decode(($data['players'][$playerid]['events']));
$teamid = $data['players'][$playerid]['team'];
$duration = $data['duration'];
$writer = new PlayerLogWriter($event, $teamid, $duration);
?>
