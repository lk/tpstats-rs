{ pkgs ? import <nixpkgs> { }, lib ? pkgs.stdenv.lib, mkShell ? pkgs.mkShell }:

let foo = null;

in mkShell rec {
  nativeBuildInputs = (with pkgs; [ pkgconfig ]);

  buildInputs = (with pkgs; [ cargo rustc rustfmt rust-analyzer pkgconfig openssl ]);

  LD_LIBRARY_PATH = lib.makeLibraryPath buildInputs;
}
