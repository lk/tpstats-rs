use serde::Deserialize;

#[derive(Deserialize, Debug)]
#[serde(rename_all = "lowercase")]
enum MapType {
    /// Capture-the-flag
    CTF,
    /// Neutral flag
    NF,
    /// Double neutral flag
    #[serde(rename = "2NF")]
    DoubleNF,
    /// Deliver-the-flag
    DTF,
    /// Marsball
    MB,
    /// Group-only maps
    #[serde(rename = "-")]
    Group,
    // Event maps
    E14,
    U14,
    H14,
    B15,
    P16,
    E16,
    S17,
}

#[derive(Deserialize, Default, Debug)]
/// TODO
struct Tiles {}

#[derive(Deserialize, Debug)]
#[serde(rename_all = "camelCase")]
pub struct Map {
    /// Map name
    name: String,
    /// Map author
    author: Option<String>,
    /// Map type
    map_type: Option<MapType>,
    /// Number of Mars balls on the map
    marsballs: u8,
    /// Map width
    width: u16,
    /// Tiles
    #[serde(rename = "tiles")]
    raw_tiles: String,
    #[serde(skip_deserializing)]
    tiles: Tiles,
}
