use serde::Deserialize;
use serde_repr::Deserialize_repr;

pub mod events;

#[derive(Deserialize_repr, PartialEq, Copy, Clone, Debug)]
#[repr(u8)]
pub enum Team {
    Red = 1,
    Blue = 2,
}

impl From<bool> for Team {
    fn from(x: bool) -> Self {
        match x {
            true => Self::Red,
            false => Self::Blue,
        }
    }
}

impl Team {
    pub fn switch(&self) -> Self {
        match self {
            Self::Red => Self::Blue,
            Self::Blue => Self::Red,
        }
    }
}

#[derive(Deserialize, Debug)]
#[serde(rename_all = "lowercase")]
pub struct Player {
    /// Authenticated and uses its reserved name
    auth: bool,
    /// Name
    name: Option<String>,
    /// Flair index
    flair: u8,
    /// Degree
    degree: u16,
    /// Score
    score: u16,
    /// Rank points awarded at the end of the game
    points: u8,
    /// Player team (unset if player joined late)
    team: Option<Team>,
    /// Events
    #[serde(rename = "events")]
    raw_events: String,
    #[serde(skip_deserializing)]
    pub events: Vec<(u32, events::Event)>,
}

impl Player {
    pub fn parse_events(&mut self, duration: u32) -> Result<(), events::EventError> {
        let decoded = base64::decode(&self.raw_events)?;
        self.events = events::parse_events(decoded, self.team, duration)?;
        Ok(())
    }
}
