pub fn main() {
    let mut game = tpstats::fetcher::fetch_game_data("3007760".to_string()).unwrap();
    for player in game.players.iter_mut() {
        player.parse_events(game.duration).unwrap();
    }
    let events = &game.players[0].events;
    for event in events {
        println!("{:?}", event)
    }
    let result = tpstats::writer::append_spreadsheet_rows(
        "1UwNPOKDRIA19DcQwj7GbVrphw08w3wENu9iJiPJ1awU",
        "credentials.json",
        "Sheet1!A1",
        vec![
           vec![ "a".to_string(), "b".to_string(), "c".to_string() ]
        ]
    );
    if let Err(error) = result {
        println!("ERROR: {:?}", error)
    };
}
