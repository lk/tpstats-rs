extern crate hyper;
extern crate hyper_rustls;
extern crate yup_oauth2 as oauth2;
extern crate google_sheets4 as sheets4;

use yup_oauth2::read_service_account_key as oauth2_read_key;
use yup_oauth2::ServiceAccountAuthenticator as oauth2_authenticator;
use sheets4::api::ValueRange;
use sheets4::Sheets;
use thiserror::Error;

#[derive(Error, Debug)] 
pub enum AppendError {
    #[error("Failed to read credentials.")]
    CredentialsError(#[from] std::io::Error),

    #[error("API client error")]
    ClientError(#[from] sheets4::Error),
}

#[tokio::main]
pub async fn append_spreadsheet_rows(
    spreadsheet_id: &str, 
    credentials: &str, 
    range: &str,
    values: Vec<Vec<String>>
) -> Result<(),AppendError> {

    let service_account_key = oauth2_read_key(credentials).await?;
    let auth = oauth2_authenticator::builder(service_account_key).build().await?;
    let https = hyper_rustls::HttpsConnector::with_native_roots();
    let client = hyper::Client::builder().build(https);
    let hub = Sheets::new(client, auth);
    
    let mut req  = ValueRange::default();
    req.major_dimension = Some(String::from("ROWS"));
    req.range = Some(String::from(range));
    req.values = Some(values);
    
    let result = hub
        .spreadsheets()
        .values_append(req, spreadsheet_id, range)
        .value_input_option("USER_ENTERED")
        .doit()
        .await?;

    println!("{:?}",result);

    Ok(())
}
